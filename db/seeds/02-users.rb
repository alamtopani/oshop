module SeedUser
  def self.seed
    user = User.find_or_initialize_by(email: "admin@example.com")
    user.username = 'adminmaster'
    user.password = 12345678
    user.password_confirmation = 12345678
    user.type = 'Admin'
    user.confirmation_token = nil
    user.confirmed_at = Time.now
    if user.save
      p = Profile.find_or_initialize_by(user_id: user.id)
      p.full_name = 'Adminmaster'
      p.birthday = Date.today
      p.gender = 'Male'
      p.phone = '123123'
      p.save
    end

    user = Member.find_or_initialize_by(email: "member1@example.com")
    user.username = 'member1'
    user.password = 12345678
    user.password_confirmation = 12345678
    user.type = 'Member'
    user.confirmation_token = nil
    user.confirmed_at = Time.now
    if user.save
      p = Profile.find_or_initialize_by(user_id: user.id)
      p.full_name = 'Member 1'
      p.birthday = Date.today
      p.gender = 'Male'
      p.phone = '123123'
      p.save
    end
  end
end
