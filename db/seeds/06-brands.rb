module SeedBrand
  require 'nokogiri' # gem install nokogiri
  require 'open-uri' # already part of your ruby install

  ALLBRANDS = ['2PK', 'A1Toys', 'Arisu', 'Artistica Jewelry', 'Baby Signature', 'Barbarockfashion', 'Billstone', 'BLXS', 'Bros', 'Central kerajinan', 'Charm', 'Cherise Paxton', 'COLDWEAR', 'Concept Gift', 'D&D Hat Collection', 'Dhewis Hanging Shop', 'Evelyn Bow', 'Fashion', 'Fashionista', 'Grandby', 'Gykaco', 'Hello Kitty', 'Istana kado', 'Istana Kado Online', 'Jack', 'Jnanacrafts', 'Jogja Craft', 'Jojo Konjac Baby Sponge', 'KAI', 'Kakuu Basic', 'Kualitas Super', 'Meilyngiftshop', 'My Style Princess', 'Myth Couture', 'Nataria', 'Natural Life', 'Omomart', 'Orange Collections', 'Ormano', 'PEAK', 'Pentatonic Music', 'Petite Lola', 'Phillipe Jourdan', 'Poplin', 'Radysa', 'RIZKY CRAFT', 'Shiena', 'Smesco Trade', 'StarHome', 'Toko49', 'Twiinkles', 'Universal China', 'Vienna', 'Vienna', 'Visse', 'Wonder Legs', 'Zoya']

  def self.seed
    ALLBRANDS.each do |b|
      brand = Brand.find_or_initialize_by(name: b)
      brand.logo = File.new("#{Rails.root}/app/assets/images/small_Yogrt.png")
      brand.save
    end
  end
end