module SeedWebSetting
  require 'nokogiri' # gem install nokogiri
  require 'open-uri' # already part of your ruby install'

  def self.seed
    web_setting = WebSetting.find_or_create_by({
      header_tags: "OnlineShop",
      footer_tags: "OnlineShop",
      contact: '(0266) 234657',
      email: 'sales@onlineshop.com',
      facebook: 'http://www.facebook.com/',
      twitter: 'http://www.twitter.com/',
      title: "OnlineShop",
      keywords: "OnlineShop's House, Situs Jual beli barang",
      description: 'Etsy was founded in June 2005 in an apartment in Brooklyn, New York to fill a need for an online community where crafters, artists and makers could sell their handmade and vintage goods and craft supplies. In the spirit of handmade, founder Rob Kalin and two friends designed the first site',
      robot: 'Follow',
      author: "OnlineShop",
      corpyright: "Stephanie's House",
      revisit: '2 Days',
      expires: 'Never',
      revisit_after: '2 Days',
      geo_placename: 'Indonesia',
      language: 'ID',
      country: 'ID',
      content_language: 'All-Language',
      distribution: 'global',
      generator: 'website',
      rating: 'general',
      target: 'global',
      search_engines: "OnlineShop, Jual Beli Barang",
      address: 'Gedung Sentra Kramat, Jalan Sentra Kramat Raya no 7-9, Senen, Jakarta Pusat',
      longitude: '-6.17933159',
      latitude: '106.84274241'
    })
    web_setting.logo = File.new("#{Rails.root}/app/assets/images/logo-symantechstudio.png")
    if web_setting.save
      faq = Faq.find_or_create_by(faqable_id: web_setting.id)
      faq.faqable_type = 'WebSetting'
      faq.payment = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed scelerisque tortor, quis cursus lacus. Sed eu sem tempor turpis auctor viverra. Morbi eleifend enim ac dui cursus vulputate. In rutrum nunc vel vulputate maximus. In elementum velit nec luctus luctus. In a eleifend ante. Sed eu ex ac dui egestas efficitur. Aenean quis massa pulvinar, accumsan mauris ut, finibus ligula. Phasellus non massa ac quam vestibulum malesuada. Vivamus eu mi ac diam rhoncus aliquam. Duis vehicula enim justo, eu ultrices elit tincidunt in. Nunc et varius enim, sed rutrum libero. Sed varius diam at nibh lacinia, ut iaculis metus tincidunt. Pellentesque ac tristique velit, et commodo tellus. Donec tincidunt turpis in eros auctor congue."
      faq.shipping = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed scelerisque tortor, quis cursus lacus. Sed eu sem tempor turpis auctor viverra. Morbi eleifend enim ac dui cursus vulputate. In rutrum nunc vel vulputate maximus. In elementum velit nec luctus luctus. In a eleifend ante. Sed eu ex ac dui egestas efficitur. Aenean quis massa pulvinar, accumsan mauris ut, finibus ligula. Phasellus non massa ac quam vestibulum malesuada. Vivamus eu mi ac diam rhoncus aliquam. Duis vehicula enim justo, eu ultrices elit tincidunt in. Nunc et varius enim, sed rutrum libero. Sed varius diam at nibh lacinia, ut iaculis metus tincidunt. Pellentesque ac tristique velit, et commodo tellus. Donec tincidunt turpis in eros auctor congue."
      faq.refund_and_exchange = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed scelerisque tortor, quis cursus lacus. Sed eu sem tempor turpis auctor viverra. Morbi eleifend enim ac dui cursus vulputate. In rutrum nunc vel vulputate maximus. In elementum velit nec luctus luctus. In a eleifend ante. Sed eu ex ac dui egestas efficitur. Aenean quis massa pulvinar, accumsan mauris ut, finibus ligula. Phasellus non massa ac quam vestibulum malesuada. Vivamus eu mi ac diam rhoncus aliquam. Duis vehicula enim justo, eu ultrices elit tincidunt in. Nunc et varius enim, sed rutrum libero. Sed varius diam at nibh lacinia, ut iaculis metus tincidunt. Pellentesque ac tristique velit, et commodo tellus. Donec tincidunt turpis in eros auctor congue."
      faq.faq = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed scelerisque tortor, quis cursus lacus. Sed eu sem tempor turpis auctor viverra. Morbi eleifend enim ac dui cursus vulputate. In rutrum nunc vel vulputate maximus. In elementum velit nec luctus luctus. In a eleifend ante. Sed eu ex ac dui egestas efficitur. Aenean quis massa pulvinar, accumsan mauris ut, finibus ligula. Phasellus non massa ac quam vestibulum malesuada. Vivamus eu mi ac diam rhoncus aliquam. Duis vehicula enim justo, eu ultrices elit tincidunt in. Nunc et varius enim, sed rutrum libero. Sed varius diam at nibh lacinia, ut iaculis metus tincidunt. Pellentesque ac tristique velit, et commodo tellus. Donec tincidunt turpis in eros auctor congue."
      faq.save

      (['bca','bni']).each do |p|
        payments = Payment.find_or_create_by(bank_name: p)
        payments.name = 'Alam Topani H'
        payments.account_number = '123214434'
        payments.paymentable_id = web_setting.id
        payments.paymentable_type = 'WebSetting'
        payments.logo = File.new("#{Rails.root}/app/assets/images/#{p}.png")
        payments.save
      end

      (1..2).each do |s|
        galleries = Gallery.find_or_create_by(title: "Title_#{s}")
        galleries.galleriable_type = 'WebSetting'
        galleries.galleriable_id = web_setting.id
        galleries.file = File.new("#{Rails.root}/app/assets/images/shooping.jpg")
        galleries.save
      end

      (['jne','pos']).each do |s|
        shipping = ShippingMethod.find_or_create_by(name: s)
        shipping.shipping_methodable_id = web_setting.id
        shipping.shipping_methodable_type = 'WebSetting'
        shipping.logo = File.new("#{Rails.root}/app/assets/images/#{s}.png")
        shipping.status = true
        shipping.save
      end
    end
  end

  def self.do_download_remote_photo(photo_url)
    io = open(URI.parse(URI.escape(photo_url)))
    def io.original_filename; base_uri.path.split('/').last; end
    io.original_filename.blank? ? nil : io
    rescue
  end
end
