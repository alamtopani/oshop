module SeedCategory
  require 'nokogiri' # gem install nokogiri
  require 'open-uri' # already part of your ruby install
  
  CATEGORIES = {
    "Accesories"=> [
      "Hats & Caps",
      "Hair Accesories",
      "Belts & Suspenders",
      "Gloves & Mittens",
      "Patches & Pins",
      "Suit & Tie Accesories"
    ],

    "Bags"=> [
      "Backpacks",
      "Diaper Bags",
      "Hand Bags",
      "Luggage & Travel",
      "Wallets & Money Clips",
      "Totes"
    ],

    "Clothing"=> [
      "T-Shirt",
      "Pants",
      "Sweaters",
      "Tops",
      "Shorts",
      "Dresses",
      "Jackets & Coats",
      "Skirts",
      "Swimwear"
    ],

    "Shoes" => [
      "Boots",
      "Sandals",
      "Slippers",
      "Booties & Crib Shoes",
      "Loafers & Slip Ons"
    ]
  }

  def self.seed
    CATEGORIES.keys.each_with_index do |category_root, index|
      category = Category.find_or_initialize_by(name: category_root)
      category.name = category_root
      category.cover_image = File.new("#{Rails.root}/app/assets/images/cover.png")
      category.save

      CATEGORIES[category.name].each do |c|
        child = Category.new
        child.parent = category
        child.name = c
        child.cover_image = File.new("#{Rails.root}/app/assets/images/cover.png")
        child.save
      end
    end
  end
end
