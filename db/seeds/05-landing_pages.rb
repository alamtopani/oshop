module SeedLandingPage
  CATEGORY = ['about_us','buy','sell','help']
  ABOUT_LANDING_PAGE = ['Tentang Kami','Kisah Penjual']
  BUY_LANDING_PAGE = ['Belanja di Kami','Cara Berbelanja','Pembayaran','Pengembalian']
  SELL_LANDING_PAGE = ['Jualan di Kami','Cara Berjualan','Gold Merchant','Beriklan']
  HELP_LANDING_PAGE = ['Syarat dan Ketentuan','Kebijakan Privasi','Pusat Resolusi','Hubungi Kami']

  def self.seed 
    CATEGORY.each do |category|
      if category['about_us']
        ABOUT_LANDING_PAGE.each do |page|
          page = LandingPage.find_or_initialize_by(title: page)
          page.category = 'about_us'
          page.status = true
          page.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed scelerisque tortor, quis cursus lacus. Sed eu sem tempor turpis auctor viverra. Morbi eleifend enim ac dui cursus vulputate. In rutrum nunc vel vulputate maximus. In elementum velit nec luctus luctus. In a eleifend ante. Sed eu ex ac dui egestas efficitur. Aenean quis massa pulvinar, accumsan mauris ut, finibus ligula. Phasellus non massa ac quam vestibulum malesuada. Vivamus eu mi ac diam rhoncus aliquam. Duis vehicula enim justo, eu ultrices elit tincidunt in. Nunc et varius enim, sed rutrum libero. Sed varius diam at nibh lacinia, ut iaculis metus tincidunt. Pellentesque ac tristique velit, et commodo tellus. Donec tincidunt turpis in eros auctor congue."
          page.save
        end
      elsif category['buy']
        BUY_LANDING_PAGE.each do |page|
          page = LandingPage.find_or_initialize_by(title: page)
          page.category = 'buy'
          page.status = true
          page.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed scelerisque tortor, quis cursus lacus. Sed eu sem tempor turpis auctor viverra. Morbi eleifend enim ac dui cursus vulputate. In rutrum nunc vel vulputate maximus. In elementum velit nec luctus luctus. In a eleifend ante. Sed eu ex ac dui egestas efficitur. Aenean quis massa pulvinar, accumsan mauris ut, finibus ligula. Phasellus non massa ac quam vestibulum malesuada. Vivamus eu mi ac diam rhoncus aliquam. Duis vehicula enim justo, eu ultrices elit tincidunt in. Nunc et varius enim, sed rutrum libero. Sed varius diam at nibh lacinia, ut iaculis metus tincidunt. Pellentesque ac tristique velit, et commodo tellus. Donec tincidunt turpis in eros auctor congue."
          page.save
        end
      elsif category['sell']
        SELL_LANDING_PAGE.each do |page|
          page = LandingPage.find_or_initialize_by(title: page)
          page.category = 'sell'
          page.status = true
          page.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed scelerisque tortor, quis cursus lacus. Sed eu sem tempor turpis auctor viverra. Morbi eleifend enim ac dui cursus vulputate. In rutrum nunc vel vulputate maximus. In elementum velit nec luctus luctus. In a eleifend ante. Sed eu ex ac dui egestas efficitur. Aenean quis massa pulvinar, accumsan mauris ut, finibus ligula. Phasellus non massa ac quam vestibulum malesuada. Vivamus eu mi ac diam rhoncus aliquam. Duis vehicula enim justo, eu ultrices elit tincidunt in. Nunc et varius enim, sed rutrum libero. Sed varius diam at nibh lacinia, ut iaculis metus tincidunt. Pellentesque ac tristique velit, et commodo tellus. Donec tincidunt turpis in eros auctor congue."
          page.save
        end
      elsif category['help']
        HELP_LANDING_PAGE.each do |page|
          page = LandingPage.find_or_initialize_by(title: page)
          page.category = 'help'
          page.status = true
          page.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed scelerisque tortor, quis cursus lacus. Sed eu sem tempor turpis auctor viverra. Morbi eleifend enim ac dui cursus vulputate. In rutrum nunc vel vulputate maximus. In elementum velit nec luctus luctus. In a eleifend ante. Sed eu ex ac dui egestas efficitur. Aenean quis massa pulvinar, accumsan mauris ut, finibus ligula. Phasellus non massa ac quam vestibulum malesuada. Vivamus eu mi ac diam rhoncus aliquam. Duis vehicula enim justo, eu ultrices elit tincidunt in. Nunc et varius enim, sed rutrum libero. Sed varius diam at nibh lacinia, ut iaculis metus tincidunt. Pellentesque ac tristique velit, et commodo tellus. Donec tincidunt turpis in eros auctor congue."
          page.save
        end
      end 
    end
  end
end