module SeedProduct
  require 'nokogiri' # gem install nokogiri
  require 'open-uri' # already part of your ruby install

  def self.seed
    url = 'https://www.blibli.com/aksesoris-wanita-lainnya/54912'
    resp = RestClient.get URI.escape(url)
    @doc  = Nokogiri::HTML(resp.body)

    @loop = @doc.css('.product-list-tumbnails').search('.fashion')

    @loop.each do |lop|
      Product.find_or_create_by!(title: lop.search('.product-title').text) do |p|
        p.user_id = Admin.last.id
        p.basic_price = 100000
        p.price = 50000
        p.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed scelerisque tortor, quis cursus lacus. Sed eu sem tempor turpis auctor viverra. Morbi eleifend enim ac dui cursus vulputate. In rutrum nunc vel vulputate maximus. In elementum velit nec luctus luctus. In a eleifend ante. Sed eu ex ac dui egestas efficitur. Aenean quis massa pulvinar, accumsan mauris ut, finibus ligula. Phasellus non massa ac quam vestibulum malesuada. Vivamus eu mi ac diam rhoncus aliquam. Duis vehicula enim justo, eu ultrices elit tincidunt in. Nunc et varius enim, sed rutrum libero. Sed varius diam at nibh lacinia, ut iaculis metus tincidunt. Pellentesque ac tristique velit, et commodo tellus. Donec tincidunt turpis in eros auctor congue."
        p.featured = true
        p.status = true
        p.category_id = Category.all.pluck(:id).shuffle.sample
        p.brand_id = Brand.all.pluck(:id).shuffle.sample
        p.weight = 1000
        p.save
        if p.save
          spec = ProductSpec.find_or_create_by!(product_id: p.id )
          spec.stock = 10
          spec.material = 'Klasic, Material'
          spec.available_size = "Test"
          spec.information = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed scelerisque tortor,"
          spec.save
            
          gallery = Gallery.find_or_create_by!(title: "Cover_#{p.id}")
          gallery.galleriable_type = 'Product'
          gallery.galleriable_id = p.id
          gallery.file = do_download_remote_photo(lop.search('.img-lazy-container').search('img').attribute('data-original').value)
          gallery.save
        end

        puts "Successfully saved #{p.title}"
      end
    end
  end

  def self.do_download_remote_photo(photo_url)
    io = open(URI.parse(URI.escape(photo_url)))
    def io.original_filename; base_uri.path.split('/').last; end
    io.original_filename.blank? ? nil : io
    rescue
  end

end