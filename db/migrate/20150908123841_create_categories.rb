class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :ancestry
      t.string :slug
      t.attachment :cover_image

      t.timestamps null: false
    end
  end
end
