class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :slug
      t.string :name
      t.attachment :logo

      t.timestamps null: false
    end
  end
end
