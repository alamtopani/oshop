class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :title
      t.string :activitiable_type
      t.integer :activitiable_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
