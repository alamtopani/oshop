class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string :full_name
      t.date :birthday
      t.string :gender
      t.integer :phone
      t.attachment :avatar
      t.string :meta_title
      t.string :meta_keywords
      t.text :meta_description
      t.attachment :cover

      t.timestamps
    end

    add_index :profiles, :user_id
  end
end
