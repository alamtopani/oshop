class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :product_id
      t.integer :order_id
      t.decimal :price
      t.integer :quantity
      t.decimal :total_price
      t.integer :user_id
      t.integer :seller_id
      t.boolean :order_item_status
      t.decimal :weight
      t.string :track_order, default: 'verification'

      t.timestamps null: false
    end

    add_index :order_items, :order_id
    add_index :order_items, :product_id
    add_index :order_items, :user_id
    add_index :order_items, :seller_id
  end
end
