class CreateShippingMethods < ActiveRecord::Migration
  def change
    create_table :shipping_methods do |t|
      t.string :name
      t.attachment :logo
      t.boolean :status
      t.string :shipping_methodable_type
      t.integer :shipping_methodable_id
      t.integer :position

      t.timestamps null: false
    end
  end
end
