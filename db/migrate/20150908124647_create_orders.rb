class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :code
      t.integer :user_id
      t.integer :order_status_id
      t.string :token
      t.decimal :total
      t.string :payment_token
      t.string :payerID
      t.date :purchased_at
      t.integer :transaction_id
      t.string :slug
      t.string :payer_name
      t.string :payer_email
      t.text :payer_address
      t.string :color
      t.string :o_size
      t.string :payer_phone
      t.string :payer_company
      t.string :payment_method
      t.string :shipping_method
      t.decimal :shipping_price
      t.string :shipping_number
      t.string :track_order, default: 'verification'
      t.string :paymentTo
      t.decimal :weight

      t.timestamps null: false
    end

    add_index :orders, :user_id
  end
end
