Rails.application.routes.draw do
  devise_for :users,
    controllers: {
      omniauth_callbacks: 'omniauth_callbacks',
      registrations: 'registrations',
      sessions: 'sessions',
      confirmations: "confirmations"
    },
    path_names: {
      sign_in:  'login',
      sign_out: 'logout',
      sign_up:  'register'
    }

  root 'publics#home'

  # get 'contact', to: 'publics#contact', as: 'contact'

  resources :products do
    member do
      match :upvote, path: 'like', via: [:get, :post, :put]
    end
  end
  resources :favorites do
    collection do
      get :wishlist
    end
  end
  resources :confirmations, only: [:new, :create]
  resources :subscribes
  resources :landing_pages
  resources :comments
  resources :inquiries
  resources :orders do
    collection do
      get :checkout
    end
    member do
      get :review
    end
  end
  resources :guest_books
  resources :order_items, only: [:create, :update, :destroy]
  resources :users do
    member do
      match :upvote, path: 'like', via: [:get, :post, :put]
    end
  end
  resources :xhrs do
    collection do
      get :cities
      get :shipment_provinces
      get :shipment_cities
      get :shipment_states
      get :shipping_price
    end
  end

  namespace :backend do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :admins
    resources :members do
      collection do
        post :do_multiple_act
      end
    end
    resources :categories do
      collection do
        post :do_multiple_act
      end
    end
    resources :brands do
      collection do
        post :do_multiple_act
      end
    end
    resources :products do
      collection do
        post :do_multiple_act
      end
    end
    resources :orders do
      collection do
        post :do_multiple_act
      end
    end
    resources :order_items
    resources :confirmations do
      collection do
        post :do_multiple_act
      end
    end
    resources :shipments do
      collection do
        post :do_multiple_act
        post :import
      end
    end
    resources :regions do
      collection do
        post :do_multiple_act
      end
    end
    resources :comments do
      collection do
        post :do_multiple_act
      end
    end
    resources :landing_pages do
      collection do
        post :do_multiple_act
      end
    end
    resources :subscribes do
      collection do
        post :do_multiple_act
      end
    end
    resources :testimonials do
      collection do
        post :do_multiple_act
      end
    end
    resources :web_settings
  end

  namespace :userpage do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :members, only: [:edit, :update, :show]
    resources :merchants, only: [:edit, :update, :show]
    resources :products
    resources :favorites
    resources :orders
    resources :order_items
    resources :inquiries
  end

  # mount Ckeditor::Engine => '/ckeditor'
  # post '/rate' => 'rater#create', :as => 'rate'

  # get '/backend',                   to: 'backend#home',                     as: 'backend_home'
  # get '/member',                    to: 'member#home',                      as: 'member_home'
  # get '/profile',                   to: 'member#profile',                   as: 'profile'
  # get 'search',                     to: 'publics#search',                   as: 'search'
  # get 'portfolio/:id',              to: 'publics#portfolio',                as: 'portfolio'
  # get "like/:id",                   to: "publics#upvote",                   as: 'like'
  # get "document_download/:id",      to: "publics#document_download",        as: 'document_download'
  # get "landing_page/:id",           to: "publics#landing_page",             as: 'landing_page'
  # get "catalog_category_child",     to: "catalogs#catalog_category_child",  as: 'catalog_category_child'
  # get "review/:id",                 to: "orders#review",                    as: 'review'

  # resources :catalogs do
  #   member do
  #     get 'comments'
  #     get 'support'
  #     get 'ticket'
  #     put "like",     to: "catalogs#upvote"
  #     match :upvote, path: 'like', via: [:get, :post, :put]
  #   end
  # end

  # resource :orders do
  #   collection do
  #     get :paid
  #     get :revoked
  #     post :ipn
  #   end
  # end

  # resources :order_items, only: [:create, :update, :destroy]
  # resources :comments
  # resources :tickets
  # resources :subscribes
end
