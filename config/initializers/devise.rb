Devise.setup do |config|
  config.mailer_sender = 'creativedesignjakarta@gmail.com'

  require 'devise/orm/active_record'
  require "omniauth-facebook"
  require 'omniauth-google-oauth2'

  config.omniauth :facebook, '259947467530808', '5d4a10a9ed5f2c2950f6ec8acc3ecb8c' #local
  config.omniauth :google_oauth2, "288327438888-tdddip5a5217pllohut5gis1ovmj6tfq.apps.googleusercontent.com", "yWn4yIgyyMA4iKBz6IXI85jC"

  config.case_insensitive_keys = [ :email, :username ]
  config.strip_whitespace_keys = [ :email, :username ]
  config.authentication_keys = [ :login ]
  config.confirmation_keys = [ :email, :username ]
  config.unlock_keys = [ :email, :username ]
  config.skip_session_storage = [:http_auth]
  config.stretches = Rails.env.test? ? 1 : 10
  config.reconfirmable = true
  config.expire_all_remember_me_on_sign_out = true
  config.password_length = 8..72
  config.reset_password_within = 6.hours
  config.sign_out_via = :delete
end