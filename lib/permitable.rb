module Permitable
	USER = [
      :id,
      :type,
      :username,
      :email,
      :password,
      :password_confirmation,
      :provider,
      :uid,
      :oauth_token,
      :oauth_expires_at,
      :slug,
      :featured,
      :verified
    ]

  ADDRESS = {
      address_attributes: [
        :id,
        :address,
        :district,
        :city,
        :province,
        :country,
        :postcode,
        :addressable_type,
        :addressable_id,
        :addressable
      ]
  }

  PROFILE = {
      profile_attributes: [
        :id,
        :user_id,
        :full_name,
        :birthday,
        :gender,
        :phone,
        :avatar,
        :cover,
        :meta_title,
        :meta_keywords,
        :meta_description
      ]
  }

  CONTACT = {
    contact_attributes: [
      :id,
      :phone,
      :handphone,
      :pin,
      :facebook,
      :twitter,
      :contactable_type,
      :contactable_id,
      :contactable,
      :website
    ]
  }

  CATEGORY = [
    :id,
    :name,
    :ancestry,
    :parent_id,
    :cover_image
  ]

  BRAND = [
    :id,
    :slug,
    :name,
    :logo
  ]

  FAQ = {
    faq_attributes: [
      :id,
      :payment,
      :shipping,
      :refund_and_exchange,
      :faq,
      :faqable_id,
      :faqable_type,
      :faqable,
      :_destroy
    ]
  }

  GUEST_BOOK = [
    :id,
    :name,
    :email,
    :phone,
    :description,
    :status,
  ]

  MERCHANT_INFO = {
    merchant_info_attributes: [
      :id,
  		:user_id,
      :title,
      :banner,
      :description,
      :policies,
      :payment,
      :shipping,
      :refund_and_exchange,
      :faq
    ]
  }

  PAYMENT = {
    payments_attributes: [
      :id,
      :name,
      :logo,
      :bank_name,
      :account_number,
      :paymentable_id,
      :paymentable_type,
      :paymentable,
      :position,
      :_destroy
    ]
  }

  PRODUCT = [
      :id,
      :slug,
      :brand_id,
      :user_id,
      :title,
      :description,
      :price,
      :status,
      :category_id,
      :featured,
      :weight,
      :indent,
      :basic_price,
      :start_at,
      :end_at
  ]

  PRODUCT_SPEC = {
      product_spec_attributes: [
        :id,
        :brand_id,
        :product_id,
        :information,
        :material,
        :stock,
        :available_size,
        :range_shipping
      ]
  }

  GALLERY = {
      galleries_attributes: [
        :id,
        :title,
        :description,
        :file,
        :galleriable_type,
        :galleriable_id,
        :galleriable,
        :position,
        :featured,
        :_destroy
      ]
  }

  REGION = [
    :id,
    :name,
    :ancestry,
    :featured,
    :parent_id,
    :image
  ]

  COMMENT = [
      :id,
      :user_id,
      :commentable_type,
      :commentable_id,
      :commentable,
      :comment,
      :status
  ]

  FAVORITE = [
      :user_id,
      :product_id,
      :favoriteable,
      :favoriteable_id,
      :favoriteable_type
  ]

  ORDER_ITEM = [
      :id,
      :product_id,
      :order_id,
      :price,
      :quantity,
      :total_price,
      :user_id,
      :seller_id,
      :order_item_status,
      :color,
      :o_size,
      :weight,
      :track_order
  ]

  ORDER = [
      :id,
      :code,
      :user_id,
      :order_status_id,
      :token,
      :total,
      :payment_token,
      :payerID,
      :ip,
      :purchased_at,
      :transaction_id,
      :slug,
      :payer_name,
      :payer_email,
      :payer_address,
      :payer_phone,
      :payer_company,
      :payment_method,
      :shipping_method,
      :shipping_price,
      :shipping_number,
      :track_order,
      :paymentTo,
      :weight
  ]

  SHIPMENT = [
      :id,
      :name,
      :province,
      :city,
      :state,
      :postal_code,
      :address,
      :weight,
      :price,
      :shipmentable_type,
      :shipmentable_id,
      :shipmentable
  ]

  CONFIRMATION = [
      :id,
      :slug,
      :user_id,
      :no_invoice,
      :name,
      :email,
      :payment_date,
      :nominal,
      :bank_account,
      :payment_method,
      :sender_name,
      :message,
      :file
  ]

  INQUIRY = [
    :user_id,
    :name,
    :email,
    :phone,
    :message,
    :inquiriable_type,
    :inquiriable_id,
    :inquiriable,
    :status,
    :ancestry,
    :parent_id,
    :owner_id
  ]

  LANDING_PAGE = [
      :title,
      :slug,
      :description,
      :status,
      :category
  ]

  SUBSCRIBE = [
      :email,
      :status
  ]

  TESTIMONIAL = [
      :user_id,
      :message,
      :status
  ]

  WEB_SETTING = [
      :id,
      :header_tags,
      :footer_tags,
      :contact,
      :email,
      :favicon,
      :logo,
      :facebook,
      :twitter,
      :title,
      :keywords,
      :description,
      :robot,
      :author,
      :corpyright,
      :revisit,
      :expires,
      :revisit_after,
      :geo_placename,
      :language,
      :country,
      :content_language,
      :distribution,
      :generator,
      :rating,
      :target,
      :search_engines,
      :address,
      :longitude,
      :latitude
  ]

  SHIPPING_METHOD = {
    shipping_methods_attributes: [
      :id,
      :name,
      :logo,
      :status,
      :shipping_methodable_type,
      :shipping_methodable_id,
      :shipping_methodable,
      :position,
      :_destroy
    ]
  }

  def self.controller(name)
    self.send name.gsub(/\W/,'_').singularize.downcase
  end

  # -----------FOR BACKEND------------

  def self.backend_user
    USER.dup.push(PROFILE.dup).push(ADDRESS.dup).push(CONTACT.dup)
  end

  def self.backend_admin
    backend_user
  end

  def self.backend_member
    backend_user
  end

  def self.backend_category
  	CATEGORY
  end

  def self.backend_brand
    BRAND
  end

  def self.backend_product
    PRODUCT.dup.push(GALLERY.dup).push(PRODUCT_SPEC.dup)
  end

  def self.backend_region
    REGION
  end

  def self.backend_comment
    COMMENT
  end

  def self.backend_order
    ORDER.push(ADDRESS.dup)
  end

  def self.backend_order_item
    ORDER_ITEM
  end

  def self.backend_shipment
    SHIPMENT
  end

  def self.backend_confirmation
    CONFIRMATION
  end

  def self.backend_landing_page
    LANDING_PAGE
  end

  def self.backend_subscribe
    SUBSCRIBE
  end

  def self.backend_testimonial
    TESTIMONIAL
  end

  def self.backend_web_setting
    WEB_SETTING.dup.push(GALLERY.dup).push(FAQ.dup).push(PAYMENT.dup).push(SHIPPING_METHOD.dup)
  end

  # -----------FOR USER------------

  def self.userpage_member
    backend_user
  end

  def self.userpage_order
    backend_order
  end

  def self.userpage_favorite
    backend_favorite
  end

  def self.userpage_inquiry
    INQUIRY
  end

  # -----------FOR FRONTEND------------

  def self.inquiry
    INQUIRY
  end

  def self.comment
    COMMENT
  end

  def self.testimonial
    backend_testimonial
  end

  def self.order
    backend_order
  end

  def self.order_item
    ORDER_ITEM
  end

  def self.confirmation
    backend_confirmation
  end

  def self.subscribe
    SUBSCRIBE
  end

  def self.guest_book
    GUEST_BOOK
  end

end
