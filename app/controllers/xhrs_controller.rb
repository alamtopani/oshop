class XhrsController < FrontendController
  include Ongkir

  def cities
    province = Region.find_by(name: params[:id])
    @cities = province ? province.children : Region.none
    render layout: false
  end

  def shipment_provinces
    get_region_ongkir_province
    @provinces = @all_province
    render layout: false
  end

  def shipment_cities
    get_region_ongkir_city(params[:id])
    @cities = @all_city
    render layout: false
  end

  def shipping_price
    require 'uri'
    require 'net/http'

    url = URI("http://api.rajaongkir.com/starter/cost")
    http = Net::HTTP.new(url.host, url.port)
    origin = '152'

    request = Net::HTTP::Post.new(url)
    request["key"] = 'fac1ef74aef3be1842823361fc17a41b'
    request["content-type"] = 'application/x-www-form-urlencoded'
    request.body = "origin=#{origin}&destination=#{params[:city].split(':')[0]}&weight=#{params[:weight]}&courier=#{params[:shipping_method]}"

    response = http.request(request)

    if params[:origin] != '0' && params[:city].present? && params[:weight].present? && params[:shipping_method].present?
      if params[:shipping_method] == 'jne'
        @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "REG"}[0]

        if @data.blank?
          @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "CTC"}[0]
        end

        if @data.blank?
          @price = 0
          return @price
        else
          @price = @data["cost"][0]["value"]
          return @price
        end
      elsif params[:shipping_method] == 'pos'
        @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "Paketpos Biasa"}[0]

        if @data.blank?
          @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "Surat Kilat Khusus"}[0]
        end

        if @data.blank?
          @price = 0
          return @price
        else
          @price = @data["cost"][0]["value"]
          return @price
        end
      end
    else
      @price = 0
      return @price
    end

    render layout: false
  end

  # def shipment_provinces
  #   @provinces = Shipment.alfa_province.where(name: params[:name]).pluck(:province).uniq
  #   render layout: false
  # end

  # def shipment_cities
  #   @cities = Shipment.alfa_city.where(province: params[:id], name: params[:shipping_method]).pluck(:city).uniq
  #   render layout: false
  # end

  # def shipment_states
  #   @states = Shipment.alfa_state.where(city: params[:id], name: params[:shipping_method]).pluck(:state).uniq
  #   render layout: false
  # end

  # def shipping_price
  #   @shipping_price = Shipment.where("LOWER(shipments.name) =?", params[:shipping_method].downcase)
  #                             .where("LOWER(shipments.province) =?", params[:province].downcase)
  #                             .where("LOWER(shipments.city) =?", params[:city].downcase)
  #                             .where("LOWER(shipments.state) =?", params[:district].downcase)
    
  #   if @shipping_price.present?
  #     @price = @shipping_price.first.price.to_i * current_order.total_weight?.to_i
  #   else
  #     @price = 0
  #   end

  #   render layout: false
  # end
end
