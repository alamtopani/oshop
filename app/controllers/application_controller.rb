class ApplicationController < ActionController::Base
  include TheUser::DeviseFilter
  protect_from_forgery with: :exception
  before_filter :web_setting, :prepare_checked, :set_current_user, :checked_profile
  
  helper_method :current_order

  protected
    def per_page
      params[:per_page] ||= 20
    end

    def page
      params[:page] ||= 1
    end

    def web_setting
      @landing_pages = LandingPage.activated.oldest
      @categories = Category.roots.latest
      @setting = WebSetting.first
    end

    def prepare_checked
      if current_user
        @latest_inquiries = Inquiry.available.received(current_user.id)
      end
    end

    def set_current_user
      User.current = current_user
    end

    def current_order
      if !session[:order_id].nil?
        Order.find(session[:order_id])
      else
        Order.new
      end
    end

    def checked_profile
      if current_user.present? && current_user.member? && current_user.profile.full_name.blank?
        unless params[:action] == 'edit' || params[:action] == 'update' || params[:controller] == 'xhrs'
          redirect_to edit_userpage_member_path(current_user), alert: 'Simply fill in your profile first!'
        end
      end
    end

    def prepare_activity(title, activitiable_type, activitiable_id, current_user)
      @activity = Activity.new
      @activity.title = title
      @activity.activitiable_type = activitiable_type
      @activity.activitiable_id = activitiable_id
      @activity.user_id = current_user.id
      @activity.save
    end
end
