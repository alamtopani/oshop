class PublicsController < FrontendController
	add_breadcrumb "Home", :root_path

	def home
    @categories = Category.roots.latest
    @featured_products = Product.featured.verify.latest
	end
end
