module Ongkir
  extend ActiveSupport::Concern

  included do
    def get_region_ongkir_province
      require 'uri'
      require 'net/http'

      url = URI("http://api.rajaongkir.com/starter/province")
      http = Net::HTTP.new(url.host, url.port)

      request = Net::HTTP::Get.new(url)
      request["key"] = 'fac1ef74aef3be1842823361fc17a41b'

      response = http.request(request)

      data = JSON.parse(response.body)["rajaongkir"]["results"]
      @all_province = data.map{|d| [d["province"], d["province_id"]+':'+d["province"]]}
    end

    def get_region_ongkir_city(province_id)
      require 'uri'
      require 'net/http'

      province_id = province_id.split(':')[0]

      url = URI("http://api.rajaongkir.com/starter/city")
      http = Net::HTTP.new(url.host, url.port)

      request = Net::HTTP::Get.new(url)
      request["key"] = 'fac1ef74aef3be1842823361fc17a41b'

      response = http.request(request)

      data = JSON.parse(response.body)["rajaongkir"]["results"]
      @all_city = data.map{|d| [d["city_name"], d["city_id"]+':'+d["city_name"]] if d["province_id"] == "#{province_id}"}.compact
    end
  end
end