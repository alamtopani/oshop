class Userpage::OrderItemsController < Userpage::ApplicationController
  defaults resource_class: OrderItem, collection_name: 'order_items', instance_name: 'order_item'

  add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Invoice Orders"

  def index
    @order_items = OrderItem.where(seller_id: current_user.id)
                            .paid_already
                            .search_by(params).latest
    @collection = @order_items.page(page).per(per_page)

    if @order_items.present?
      @balance = @order_items.paid.pluck(:total_price).sum - (@order_items.paid.pluck(:total_price).sum * 2.5/100)
      @sold_items = @order_items.paid.pluck(:quantity).sum
    end
  end

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

end