class Userpage::ApplicationController < Backend::ResourcesController
  layout 'user'

  before_filter :authenticate_user!
  before_filter :prepare_count!
  protect_from_forgery with: :exception

  def authenticate_user!
    unless current_user.present? && (current_user.member?)
      redirect_to root_path, alert: "Can't Access this page"
    end
  end

  protected
    def prepare_count!

    end

    def per_page
      params[:per_page] ||= 20
    end

    def page
      params[:page] ||= 1
    end
end
