class Userpage::MembersController < Userpage::ApplicationController
  defaults resource_class: Member, collection_name: 'members', instance_name: 'member'

  add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "Users"

  def edit
    add_breadcrumb "Edit"
    super
  end
  
  def update
    prepare_activity("You are successfully, change your profile!", 'Member', resource.id, current_user) if current_user.present?
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back}
      else
        format.html {redirect_to :back, errors: resource.errors.full_messages}
      end
    end
  end
end
