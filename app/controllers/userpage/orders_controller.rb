class Userpage::OrdersController < Userpage::ApplicationController
  defaults resource_class: Order, collection_name: 'orders', instance_name: 'order'

  add_breadcrumb "Dashboard", :userpage_dashboard_path
  add_breadcrumb "My Orders", :collection_path

  def index
    @orders = current_user.orders.without_in_progress.latest.search_by(params)
    @collection = @orders.page(page).per(per_page)
  end

  def show
    add_breadcrumb "#{params[:id]}"
    super
  end

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

end