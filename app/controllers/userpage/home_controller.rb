class Userpage::HomeController < Userpage::ApplicationController
  add_breadcrumb "Dashboard", :userpage_dashboard_path

  def dashboard
    @activities = current_user.activities.latest.page(page).per(per_page)
  end

end
