class ConfirmationsController < FrontendController
  def new
    @confirmation = Confirmation.new
  end

	def create
    check_invoice(params[:confirmation][:no_invoice])

    if @invoice.present?
      @confirmation = Confirmation.new(permitted_params)
      @confirmation.user_id = current_user.id
      if @confirmation.save
        UserMailer.send_confirmation(@confirmation).deliver
        prepare_activity("Allready sended confirmation no invoice #{params[:confirmation][:no_invoice]}!", 'Confirmation', @confirmation.id, current_user) if current_user.present?
        redirect_to :back, notice: 'Your Confirmation Successfully Sended!'
      else
        flash[:errors] = @confirmation.errors.full_messages
        redirect_to :back
      end
    else
      redirect_to :back, alert: 'No Invoice was not found!'
    end
	end

  private
    def permitted_params
      params.require(:confirmation).permit(Permitable.controller(params[:controller]))
    end

    def check_invoice(no_invoice)
      @no_invoice = no_invoice
      @invoice = Order.where("LOWER(orders.code) LIKE LOWER('#{@no_invoice}')").last
    end
end
