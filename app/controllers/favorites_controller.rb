class FavoritesController < FrontendController
	def wishlist
    checking = Favorite.where(favoriteable_id: params[:id], favoriteable_type: params[:type], user_id: current_user.id)

    if checking.present?
      redirect_to :back, alert: "This is already on your wishlist"
    else
      @wishlist                   = Favorite.new
      @wishlist.user_id           = current_user.id
      @wishlist.favoriteable_id   = params[:id]
      @wishlist.favoriteable_type = params[:type]

      if @wishlist.save
        prepare_activity("Allready saved item #{@wishlist.favoriteable.title} to wishlists!", 'Favorite', @wishlist.id, current_user) if current_user.present?
        redirect_to :back, notice: "Was successfully add to your wishlist!"
      else
        redirect_to :back, alert: "Was not successfully add to wishlist!"
      end
    end
  end
end
