class SubscribesController < FrontendController
	def create
    @subscribe = Subscribe.new(permitted_params)
    @subscribe.status = true
    if @subscribe.save
      redirect_to :back, notice: 'Your Email Successfully Registered'
    else
      flash[:errors] = @subscribe.errors.full_messages
      redirect_to :back
    end
	end

  private
    def permitted_params
      params.require(:subscribe).permit(Permitable.controller(params[:controller]))
    end
end
