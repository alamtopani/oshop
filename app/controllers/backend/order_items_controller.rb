class Backend::OrderItemsController < Backend::ApplicationController
  defaults resource_class: OrderItem, collection_name: 'order_items', instance_name: 'order_item'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Order Items", :collection_path

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end
end
