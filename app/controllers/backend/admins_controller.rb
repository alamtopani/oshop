class Backend::AdminsController < Backend::ApplicationController
  defaults resource_class: Admin, collection_name: 'admins', instance_name: 'admin'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Admins"

  def index
    @collection = collection.latest.page(page).per(per_page)
  end
end
