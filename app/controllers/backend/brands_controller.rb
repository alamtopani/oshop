class Backend::BrandsController < Backend::ApplicationController
  defaults resource_class: Brand, collection_name: 'brands', instance_name: 'brand'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Brands", :collection_path

  include MultipleAction

  def index
    @collection = collection.alfa.page(page).per(per_page)
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
