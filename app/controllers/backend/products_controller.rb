class Backend::ProductsController < Backend::ApplicationController
  defaults resource_class: Product, collection_name: 'products', instance_name: 'product'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Products", :collection_path

  include MultipleAction
  before_action :prepare_categories, only: [:new, :edit]

  def index
    @collection = collection
  end

  def create
    build_resource
    resource.user_id = current_user.id
    create! do |format|
      if resource.errors.empty?
        # prepare_price
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

  def update
    resource.user_id = current_user.id
    update! do |format|
      if resource.errors.empty?
        # prepare_price
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params).latest.page(page).per(per_page)
    end

    def prepare_categories
      @categories = Category.latest.pluck(:name)
    end

    def prepare_price
      resource.price = params[:product][:price].to_i + (params[:product][:price].to_i * 2.5/100)
      resource.basic_price = params[:product][:price]
      resource.save
    end
end
