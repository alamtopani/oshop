class Backend::HomeController < Backend::ApplicationController
  add_breadcrumb "Dashboard", :backend_dashboard_path

  def dashboard
    @people_order_in_month = OrderItem.bonds.paid.paid_already.group_by_month("order_items.created_at", format: "%b %Y").count
    @order_default = Order.search_by(params)
    @order_in_progress = @order_default.in_progress
    @order_verification = @order_default.verification
    @order_paid = @order_default.paid
    @order_cancelled = @order_default.cancelled

    @reports_collect = @order_default.paid.latest
    @reports = @reports_collect.page(page).per(per_page)
  end
end
