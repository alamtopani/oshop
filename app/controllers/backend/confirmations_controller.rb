class Backend::ConfirmationsController < Backend::ApplicationController
  defaults resource_class: Confirmation, collection_name: 'confirmations', instance_name: 'confirmation'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Confirmations", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def show
    @order = Order.find_by(code: resource.no_invoice)
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
