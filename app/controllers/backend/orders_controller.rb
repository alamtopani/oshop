class Backend::OrdersController < Backend::ApplicationController
  defaults resource_class: Order, collection_name: 'orders', instance_name: 'order'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Orders", :collection_path

  include MultipleAction

  def index
    @collection = collection.without_in_progress.latest.page(page).per(per_page)
  end

  def show
    @confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{resource.code}')").last
  end

  def update
    update! do |format|
      if resource.errors.empty?
        resource.cancellation
        format.html {redirect_to :back}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
