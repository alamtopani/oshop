class Backend::ShipmentsController < Backend::ApplicationController
  defaults resource_class: Shipment, collection_name: 'shipments', instance_name: 'shipment'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Shipments", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def update
    update! do |format|
      if resource.errors.empty?
        cancellation(resource)
        format.html {redirect_to :back}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

  def import
    if params[:commit] == 'Import'
      @shipment = Shipment.import(params[:file], params[:shipping])
      if @shipment
        redirect_to :back, notice: "Shipments Imported Successfully!"
      else
        redirect_to :back, error: 'Error To Imported!'
      end
    elsif params[:commit] == 'Destroy'
      @shipments = Shipment.where(name: params[:shipping])
      if @shipments.destroy_all
        redirect_to :back, notice: "All Shipments #{params[:shipping]} Successfully Deleted!"
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
