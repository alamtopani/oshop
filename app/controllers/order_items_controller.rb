class OrderItemsController < FrontendController
  before_action :prepare_order

  def create
    @order_item = @order.order_items.new(permitted_params.merge(quantity: 1))
    @order.save
    session[:order_id] = @order.id
  end

  def update
    @order_item = @order.order_items.find(params[:id])
    @order_item.weight = @order_item.product.weight * params[:order_item][:quantity].to_i
    @order_item.update_attributes(permitted_params)
    @order_items = @order.order_items
  end

  def destroy
    @order_item = @order.order_items.find(params[:id])
    @order_item.destroy
    @order_items = @order.order_items
  end
  
  private
    def prepare_order
      @order = current_order
    end

    def permitted_params
      params.require(:order_item).permit(Permitable.controller(params[:controller]))
    end

end