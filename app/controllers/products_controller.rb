class ProductsController < FrontendController
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Products", :products_path
  
  before_action :prepare_order, except: [:index, :upvote]

  def index
    categories
    brands
    @product_all = collection.latest.search_by(params)
    @products = @product_all.page(page).per(per_page)
  end

  def show
    add_breadcrumb "#{params[:id]}"

    resource
    @user = resource.user
    @user_products = @user.products.verify.latest.limit(5)
    @profile = @user.profile
    @address = @user.address
    @contact = @user.contact
    impressionist(@product, 'message')

    @favorites_size = @product.favorites.size
    @comments_size = @product.comments.size
    @comments = @product.comments.latest.page(page).per(per_page)

    if session[:order_id]
      check_order = Order.find(session[:order_id])
      @check_order = check_order.order_items.where(product_id: @product.id)
    end
  end

  def upvote
    @link = Product.find params[:id]
    @success = @link.upvote_by current_user
    if @success
      redirect_to :back, alert: "Your already like this item!"
    else
      redirect_to :back, error: "Your not success like this item!"
    end
  end

  private
    def collection
      @products = Product.verify
    end

    def resource
      @product = Product.find params[:id]
    end

    def categories
      @categories = Category.roots.oldest
    end

    def brands
      @brands = Brand.alfa
    end

    def prepare_order
      @order_item = current_order.order_items.new
    end
end