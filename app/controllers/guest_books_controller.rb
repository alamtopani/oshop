class GuestBooksController < FrontendController

  def create
    @guest_book = GuestBook.new(permitted_params)

    respond_to do |format|
      if @guest_book.save
        UserMailer.send_guest_book(@guest_book).deliver
        prepare_activity("Allready sended guest book!", 'GuestBook', @guest_book.id, current_user) if current_user.present?
        format.html {redirect_to :back, notice: 'Your guest book was successfully sent'}
      else
        format.html {redirect_to :back, errors: @guest_book.errors.full_messages}
      end
    end
  end

  private

    def permitted_params
      params.require(:guest_book).permit(Permitable.controller(params[:controller]))
    end
end
