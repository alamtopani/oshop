class UsersController < FrontendController
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Merchants"

  def upvote
    @link = User.find params[:id]
    @success = @link.upvote_by current_user
    if @success
      redirect_to :back, alert: "Your already like this item!"
    else
      redirect_to :back, error: "Your not success like this item!"
    end
  end
end