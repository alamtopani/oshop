class OrdersController < FrontendController
  add_breadcrumb "Home", :root_path
  
  def index
    add_breadcrumb "My Order Items", orders_path

    @order = current_order
    @order_items = @order.order_items.latest
    @shipping_methods = ShippingMethod.activated
  end

  def checkout
    prepare_shipping_price(params[:order][:shipping_method], params[:order][:address_attributes][:city], current_order.total_weight?)
    if @price <= 0
      redirect_to orders_path, alert: 'Delivery to this area is not available!'
    else
      if order = Order.pay(current_user, current_order, prepare_shipping_address)
        order.shipping_price = @price
        if order.update(permitted_params)
          session[:order_id] = nil if current_order
          update_address(order)
          UserMailer.order_checkout(order).deliver
          prepare_activity("Allready checkout order Invoice #{order.code}!", 'Order', order.id, current_user) if current_user.present?
          redirect_to review_order_path(order.payment_token), notice: 'Thank you for ordering our products, the following is an invoice from your shopping cart that has been successfully ordered, please make a payment to soon and your payment confirmation to us!'
        end
      else
        redirect_to orders_path, alert: 'Something went wrong. Please try again.'
      end
    end
  end

  def review
    add_breadcrumb "Success complete order"

    @order = Order.find_by(payment_token: params[:id])
    unless @order && current_user
      redirect_to root_path, notice: 'You do not have permission to access this page'
    end
  end

  private
    def permitted_params
      params.require(:order).permit(Permitable.controller(params[:controller]))
    end

    def prepare_shipping_address
      address = [
        params[:order][:address_attributes][:province].split(':')[1],
        params[:order][:address_attributes][:city].split(':')[1],
        params[:order][:address_attributes][:address],
        params[:order][:address_attributes][:postcode]
      ].select(&:'present?').join(', ')
    end

    def update_address(order)
      address = order.address
      address.province = params[:order][:address_attributes][:province].split(':')[1]
      address.city = params[:order][:address_attributes][:city].split(':')[1]
      address.save
    end

    def prepare_shipping_price(shipping_method, city, weight)
      require 'uri'
      require 'net/http'

      url = URI("http://api.rajaongkir.com/starter/cost")
      http = Net::HTTP.new(url.host, url.port)
      origin = '152'

      request = Net::HTTP::Post.new(url)
      request["key"] = 'fac1ef74aef3be1842823361fc17a41b'
      request["content-type"] = 'application/x-www-form-urlencoded'
      request.body = "origin=#{origin}&destination=#{city.split(':')[0]}&weight=#{weight.to_s}&courier=#{shipping_method}"

      response = http.request(request)

      if origin != '0' && city.present? && weight.present? && shipping_method.present?
        if shipping_method == 'jne'
          @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "REG"}[0]

          if @data.blank?
            @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "CTC"}[0]
          end

          if @data.blank?
            @price = 0
            return @price
          else
            @price = @data["cost"][0]["value"]
            return @price
          end
        elsif shipping_method == 'pos'
          @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "Paketpos Biasa"}[0]

          if @data.blank?
            @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "Surat Kilat Khusus"}[0]
          end
          
          if @data.blank?
            @price = 0
            return @price
          else
            @price = @data["cost"][0]["value"]
            return @price
          end
        end
      else
        @price = 0
        return @price
      end
    end
end









