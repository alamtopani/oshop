class UserMailer < ActionMailer::Base
  include ActionView::Helpers::NumberHelper
  helper :application

  default from: 'creativedesignjakarta@gmail.com'
  default to: 'creativedesignjakarta@gmail.com'

  def welcome_join(user)
    @url = "#{root_url}"
    @setting = WebSetting.first
    @user = user
    mail(to: user.email, subject: 'Welcome join to PROPEDIA')
  end

  def order_checkout(order)
    @url = "#{root_url}"
    @setting = WebSetting.first
    @user = order.user
    @order = order
    mail(from: @user.email, subject: "Order Product #{@order.code}")
  end

  def send_guest_book(guest_book)
    @url = "#{root_url}"
    @setting = WebSetting.first
    @guest_book = guest_book
    mail(from: @guest_book.email, subject: 'Notification new Guest book from user')
  end

  def send_comment(comment, url)
    @url = "#{root_url}"
    @product_url = "#{url}"
    @setting = WebSetting.first
    @user = comment.user
    @comment = comment
    mail(from: @user.email,subject: "Comment Product")
  end

  def send_confirmation(confirmation)
    @url = "#{root_url}"
    @setting = WebSetting.first
    @confirmation = confirmation
    mail(from: @confirmation.email, subject: "Confirmation order - #{@confirmation.code}")
  end

end
