module ApplicationHelper
  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", errors: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    if flash.present?
      content_tag(:div, class: 'section-alert') do
        flash.each do |msg_type, message|
          concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} alert-dismissible", role: 'alert') do
            concat(content_tag(:button, class: 'close', data: { dismiss: 'alert' }) do
              concat content_tag(:span, '&times;'.html_safe, 'aria-hidden' => true)
              concat content_tag(:span, 'Close', class: 'sr-only')
            end)
            if flash[:errors].present?
              message.each do |word|
                concat "<li>#{word}</li>".html_safe
              end
            else
              concat "#{message}".html_safe
            end
          end)
        end
        nil
      end
    end
  end

  def active_sidebar?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'open'
    end if controller_name == controller.to_s
  end

  def active_sidebar_active?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'active'
    end if controller_name == controller.to_s
  end

  def checking_wishlist(id, current_user, url)
    checking = Favorite.where(favoriteable_id: id, favoriteable_type: 'Product', user_id: current_user)

    if checking.present?
      return '<i class="fa fa-heart set active"></i>'
    else
      return "<i class='fa fa-heart-o set click-wishlist' url='#{url}'></i>"
    end
  end

  def user_edit_url(type, current_user)
    if type == 'Member'
      edit_userpage_member_path(current_user)
    elsif type == 'Merchant'
      edit_userpage_merchant_path(current_user)
    end
  end

  def get_currency(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
  end

  def sort_by
    @sort_by ||=
    [
      ["Newest Items", "latest", data: { url: "#{products_url(params.merge(sort_by: 'latest'))}" }],
      ["Oldest Items", "oldest", data: { url: "#{products_url(params.merge(sort_by: 'oldest'))}" }],
      ["Price: low to high", "low_to_high", data: { url: "#{products_url(params.merge(sort_by: 'low_to_high'))}" }],
      ["Price: high to low", "high_to_low", data: { url: "#{products_url(params.merge(sort_by: 'high_to_low'))}" }]
    ]
  end
end
