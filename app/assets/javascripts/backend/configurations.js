$(document).ready(function() {
  $(".select2").select2({
  	maximumSelectionLength: 2
  });

  $('.choice-gallery-form .input-file').on('change', function(index){
    if($(this).length){
      previewFile(this, $(this).parent('li').find('.show-choice'));
    }
  });

  $('.provinces_select').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/cities?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
      })
    }
  });

  $('.provinces_select_tag').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/cities?id='+ $(this).val(), function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
      })
    }
  })

});

function previewFile(input, imageHeader) {
  var preview = imageHeader[0];
  var file    = input.files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}
