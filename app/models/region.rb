class Region < ActiveRecord::Base
  include ScopeBased
  include Tree
  scope :featured, ->{where(featured: true)}
  scope :alfa, ->{order(name: :asc)}

  has_attached_file :image, styles: {
                      large:    '512x512>',
                      medium:   '256x256>',
                      small:    '128x128>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :image, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

end
