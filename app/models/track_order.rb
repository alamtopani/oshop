class TrackOrder < EnumerateIt::Base
  associate_values(
    :verification   => ['verification', 'verification'],  
    :packaging      => ['are in packaging', 'are in packaging'],   
    :shipped        => ['have been shipped', 'have been shipped'],
    :received       => ['have been received', 'have been received']
  )
end