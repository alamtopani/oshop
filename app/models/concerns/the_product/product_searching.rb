module TheProduct
  module ProductSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(products.code) LIKE LOWER(:key)",
          "LOWER(products.title) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)",
          "LOWER(users.email) LIKE LOWER(:key)",
          "LOWER(profiles.full_name) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_category(_category)
        return if _category.blank?
        where("categories.name = ?", _category)
      end

      def by_brand(_brand)
        return if _brand.blank?
        where("brands.name = ?", _brand)
      end

      def sort_by(_sort)
        return if _sort.blank?
        self.send(_sort)
      end

      def price_range(_price_range)
        return if _price_range.blank?
        price = _price_range.split(";")
        where("products.price >=?", price[0].to_i).where("products.price <=?", price[1].to_i)
      end

      def search_by(options={})
      
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:category].present?
          results = results.by_category(options[:category])
        end

        if options[:brand].present?
          results = results.by_brand(options[:brand])
        end

        if options[:sort_by].present?
          results = results.sort_by(options[:sort_by])
        end

        if options[:price_range].present?
          results = results.price_range(options[:price_range])
        end

        return results
      end

    end
  end
end
