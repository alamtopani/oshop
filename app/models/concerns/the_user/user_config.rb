module TheUser
	module UserConfig
		extend ActiveSupport::Concern

		included do
			devise  :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, #:confirmable,
	         		:validatable, :lockable, :timeoutable, :omniauthable, omniauth_providers: [:facebook, :google_oauth2], :authentication_keys => [:login]

	    after_initialize :after_initialized
	    # ratyrate_rater
	    # acts_as_voter

	    has_one :profile, foreign_key: 'user_id', dependent: :destroy
	    has_one :address, as: :addressable, dependent: :destroy
	    has_one :contact, as: :contactable, dependent: :destroy
	    has_many :comments, foreign_key: 'user_id', dependent: :destroy
		  has_many :favorites, foreign_key: 'user_id', dependent: :destroy
		  has_many :orders, foreign_key: 'user_id', dependent: :destroy
		  has_many :order_items, foreign_key: 'seller_id', dependent: :destroy
		  has_many :testimonials, foreign_key: 'user_id', dependent: :destroy
		  has_one :subscribe, foreign_key: 'email', dependent: :destroy
		  has_many :inquiries, foreign_key: 'user_id', dependent: :destroy
		  has_many :my_inquiries, foreign_key: 'owner_id', class_name: 'Inquiry', dependent: :destroy
		  has_many :confirmations, foreign_key: 'user_id', dependent: :destroy
		  has_many :products, foreign_key: 'user_id', dependent: :destroy
		  has_many :activities, foreign_key: 'user_id', dependent: :destroy

		  accepts_nested_attributes_for :profile, reject_if: :all_blank
		  accepts_nested_attributes_for :address, reject_if: :all_blank
		  accepts_nested_attributes_for :contact, reject_if: :all_blank
		  accepts_nested_attributes_for :comments, reject_if: :all_blank
		  accepts_nested_attributes_for :favorites, reject_if: :all_blank
		  accepts_nested_attributes_for :orders, reject_if: :all_blank
		  accepts_nested_attributes_for :order_items, reject_if: :all_blank
		  accepts_nested_attributes_for :subscribe, reject_if: :all_blank
		  accepts_nested_attributes_for :testimonials, reject_if: :all_blank
		  accepts_nested_attributes_for :inquiries, reject_if: :all_blank
		  accepts_nested_attributes_for :my_inquiries, reject_if: :all_blank
		  accepts_nested_attributes_for :confirmations, reject_if: :all_blank
		  accepts_nested_attributes_for :products, reject_if: :all_blank

		  validates_uniqueness_of :username, :email
		  before_save :downcase_username, :prepare_code

		  scope :member, ->{ where(type: 'Member') }
		  scope :admin, ->{ where(type: 'Admin') }
		  scope :featured, ->{ where(featured: true) }

		  scope :latest, -> {order(created_at: :desc)}
			scope :oldest, -> {order(created_at: :asc)}

			scope :bonds, -> { eager_load(:profile, :address) }

		end

		def admin?
			self.type == "Admin"
		end

		def member?
			self.type == "Member"
		end

		def verified?
	  	if self.verified == true
	  		return "<i class='fa fa-check-circle-o'></i> Verified".html_safe
		  else
		  	return "<i class='fa fa-warning'></i> Not Verified".html_safe
		  end
	  end

	  def featured?
	  	if self.featured == true
	  		return "<i class='fa fa-check'></i> Featured".html_safe
		  else
		  	return "<i class='fa fa-warning'></i> Not Featured".html_safe
		  end
	  end

		def change_featured_status!
	    if self.featured
	      self.featured = false
	      self.save
	    elsif !self.featured
	      self.featured = true
	      self.save
	    end
	  end

	  def featured_status
	    self.featured ? 'Featured' : 'Un Featured'
	  end

		protected
			def prepare_code
				self.code = SecureRandom.hex(3) if self.code.blank?
			end

			def after_initialized
				self.profile = Profile.new if self.profile.blank?
				self.address = Address.new if self.address.blank?
				self.contact = Contact.new if self.contact.blank?
			end

			def downcase_username
				self.username = self.username.downcase if username_changed?
			end
	end
end
