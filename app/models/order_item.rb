class OrderItem < ActiveRecord::Base
  belongs_to :product, foreign_key: 'product_id'
  belongs_to :order, foreign_key: 'order_id'
  belongs_to :user, foreign_key: 'user_id'
  belongs_to :user, foreign_key: 'seller_id'

  include ScopeBased
  include TheOrder::OrderItemSearching

  scope :bonds, -> {
    eager_load(:product, :order)
  }
  scope :paid, ->{where("orders.order_status_id =?", 3)}
  scope :paid_already, ->{where(order_item_status: true)}

  before_save :finalize
  
  def price
    if persisted?
      self[:price]
    else
      product.price
    end
  end

  def total_price
    price * quantity
  end

  def saller
    product.user.id
  end

  def quantity_reduced
    product = self.product
    product_spec = product.product_spec
    product_spec.stock = product_spec.stock - self.quantity
    product_spec.save
  end

  def quantity_cancellation
    product = self.product
    product_spec = product.product_spec
    product_spec.stock = product_spec.stock + self.quantity
    product_spec.save
  end

  def check_quantity
    self.product.product_spec.stock
  end

  private 
    def product_present
      if product.nil?
        errors.add(:product, "is not valid or is not active.")
      end
    end

    def order_present
      if order.nil?
        errors.add(:order, "is not a valid order.")
      end
    end

    def finalize
      self[:price] = price
      self[:total_price] = quantity * self[:price]
      self.user_id = User.current.id
      self.seller_id = saller
    end
end
