class Address < ActiveRecord::Base
	belongs_to :addressable, polymorphic: true

  def place_info
    [address, postcode, city, province].select(&:'present?').join(', ')
  end
end
