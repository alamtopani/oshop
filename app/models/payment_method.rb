class PaymentMethod < EnumerateIt::Base
  associate_values(
    :atm    => ['ATM - Payment Point', 'ATM - Payment Point']
  )
end