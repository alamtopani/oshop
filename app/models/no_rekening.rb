class NoRekening < EnumerateIt::Base
  associate_values(
    :bca    => ['BCA 3423235236624 An. Hanum Pratiwi', 'bca.png'],
    :bri    => ['BRI 3423235236624 An. Hanum Pratiwi', 'bri.png']
  )
end