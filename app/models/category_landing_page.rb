class CategoryLandingPage < EnumerateIt::Base
  associate_values :about_us, :buy, :sell, :help
end
