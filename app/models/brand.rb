class Brand < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  scope :alfa, -> {order(name: :asc)}
  include ScopeBased

  has_many :products, foreign_key: 'brand_id'

  has_attached_file :logo,
                    styles: {
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :logo, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }
end
