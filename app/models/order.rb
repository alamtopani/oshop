class Order < ActiveRecord::Base
  before_create :prepare_code
  extend FriendlyId
  friendly_id :code, use: [:slugged, :finders]
  
  after_initialize :after_initialized
  belongs_to :user, foreign_key: 'user_id'

  has_many :order_items, foreign_key: 'order_id', dependent: :destroy
  accepts_nested_attributes_for :order_items, reject_if: :all_blank, allow_destroy: true

  has_one :address, as: :addressable, dependent: :destroy
  accepts_nested_attributes_for :address, reject_if: :all_blank

  include ScopeBased
  include TheOrder::OrderSearching

  scope :without_in_progress, -> {where.not(order_status_id: 1)}
  scope :in_progress, -> {where(order_status_id: 1)}
  scope :verification, -> {where(order_status_id: 2)}
  scope :paid, -> {where(order_status_id: 3)}
  scope :cancelled, -> {where(order_status_id: 4)}
  scope :bonds, -> {
    eager_load(:user)
  }

  before_create :set_order_status
  before_save :update_subtotal

  def total_weight?
    if self.order_items.present?
      weight = self.order_items.pluck(:weight).sum
      if weight < 1
        total = 1
      else
        total = weight.to_i
      end
    end
  end

  def total_price?
    if self.total.present? && self.shipping_price.present?
      total_price = self.total.to_i + self.shipping_price.to_i
    else
      total_price = self.total.to_i
    end
  end

  def total
    order_items.collect { |oi| oi.valid? ? (oi.quantity * oi.price) : 0 }.sum
  end

  def status?
    return '<span class="label label-primary">In Progress</span>'.html_safe if self.order_status_id == 1
    return '<span class="label label-warning">Verification</span>'.html_safe if self.order_status_id == 2
    return '<span class="label label-success">Paid</span>'.html_safe if self.order_status_id == 3
    return '<span class="label label-danger">Cancelled</span>'.html_safe if self.order_status_id == 4
  end

  def in_progress?
    self.order_status_id == 1
  end

  def paid?
    self.order_status_id == 2
  end

  def self.pay(user, ordered, payerAddress)
    begin
      order = self.find_by_code(ordered.code)
      order.payment_token = SecureRandom.urlsafe_base64(nil, false)
      order.payerID = user.username
      order.payer_email = user.email
      order.payer_address = payerAddress
      order.purchased_at = Time.now
      order.order_status_id = 2
      if order
        order.order_items.map.each do |item|
          item.order_item_status = true
          item.quantity_reduced
          item.save
        end
      end
      order.save
      return order
    rescue
      false
    end
  end

  def cancellation
    if self.order_status_id == 4
      self.order_items.each do |item|
        item.quantity_cancellation
      end
    end
  end

  private
    def after_initialized
      self.address = Address.new if self.address.blank?
    end

    def set_order_status
      self.order_status_id = 1
      self.user_id = User.current.id
    end

    def update_subtotal
      self[:total] = total
    end

    def prepare_code
      self.code = 'ODR'+ 4.times.map{Random.rand(10)}.join
    end
end
