class ProductField < ActiveRecord::Base
  belongs_to :product_fieldable, polymorphic: true

  include ScopeBased
end
