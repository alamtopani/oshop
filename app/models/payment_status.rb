class PaymentStatus < EnumerateIt::Base
  associate_values(
    :in_progress    => [1, 'In Progress'], 
    :verification   => [2, 'Verification'],  
    :paid           => [3, 'Paid'],   
    :cancelled      => [4, 'Cancelled']
  )
end