class Category < ActiveRecord::Base
	include Tree
	extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  has_many :products, foreign_key: 'category_id'

	include ScopeBased
	scope :alfa, -> {order("name ASC")}

  has_attached_file :cover_image,
                    styles: {
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :cover_image, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }
end
