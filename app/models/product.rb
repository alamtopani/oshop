class Product < ActiveRecord::Base
	extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  is_impressionable
  acts_as_votable
  after_initialize :after_initialized, :populate_galleries
  before_create :prepare_code

  include TheProduct::ProductScope
  include TheProduct::ProductConfig
  include TheProduct::ProductSearching
  include ScopeBased

  def discount?
    if self.basic_price < 1
      return "0%"
    else
      price = (self.basic_price - self.price) * 100/self.basic_price
      return "#{price.to_i}%"
    end
  end

  def score
    self.get_upvotes.size
  end

  def have_sold?
    self.order_items.bonds.paid.paid_already.pluck(:quantity).sum || 0
  end
  
  private
    def prepare_code
      self.code = SecureRandom.hex(3) if self.code.blank?
    end

    def after_initialized
      self.product_spec ||= ProductSpec.new if self.product_spec.blank?
    end

    def populate_galleries
      if self.galleries.length < 6
        [
          'Cover',
          'Image1',
          'Image2',
          'Image3',
          'Image4',
          'Image5',
        ].each_with_index do |media_title, index|
          _galery = self.galleries.select{|g| g.title.to_s.downcase == media_title.downcase}.first
          unless _galery
            self.build_image(media_title, index+1)
          else
            _galery.position = index+1
          end
        end
      end if self.new_record?
    end
end