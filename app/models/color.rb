class Color < EnumerateIt::Base
  associate_values(
    :beige    => ['Beige', 'Beige'], 
    :black    => ['Black', 'Hitam'],  
    :blue     => ['Blue', 'Biru'],   
    :brown    => ['Brown', 'Coklat'],
    :gold     => ['Gold', 'Emas'],  
    :green    => ['Green', 'Hijau'],  
    :grey     => ['Grey', 'Abu-Abu'],  
    :magenta  => ['Magenta', 'Magenta'],
    :maroon   => ['Maroon', 'Merah Maroon'],
    :orange   => ['Orange', 'Oranye'],
    :pink     => ['Pink', 'Merah Muda'],
    :purple   => ['Purple', 'Ungu'],
    :red      => ['Red', 'Merah'],
    :silver   => ['Silver', 'Perak'], 
    :white    => ['White', 'Putih'], 
    :yellow   => ['Yellow', 'Kuning'],
  )
end